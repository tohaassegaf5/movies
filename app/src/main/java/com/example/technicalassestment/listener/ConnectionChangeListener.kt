package com.example.technicalassestment.listener

interface ConnectionChangeListener {
    fun onConnectionChange(isConnected:Boolean)
}