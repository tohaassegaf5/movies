package com.example.technicalassestment.util

import androidx.annotation.Keep

@Keep
class Constant private constructor() {
    object Network {
        const val TIME_OUT = 180L
    }

    object EXTRAS {
        const val EXTRAS_TOOLBAR_TITLE = "toolbar_title"
        const val EXTRAS_MOVIE_DATA = "movie_data"
    }

    object Config {
        const val APP_PREFERENCE = "application_pref"
        const val KEY = "api_key"
        const val API_KEY = "2335dcc1940c0babc2a2871f01f3506f"
    }

    object ApiResponse {
        const val UNAUTH = 401
        const val MAINTENANCE = 501
        const val BLACKLIST = 503
        const val OBSOLETE = 502
        const val SUCCESS = 200
    }

}