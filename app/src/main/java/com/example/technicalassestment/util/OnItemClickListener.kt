package com.example.technicalassestment.util

import android.view.View

interface OnItemClickListener {
    fun onItemClicked(view: View, position: Int)
}