package com.example.technicalassestment.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

class Product {
    @Parcelize
    data class Data(
        var name: String = "",
        var stock: Int = 0,
        var price: Int = 0,
        var totalPrice: Int = 0,
        var quantity: Int = 0
    ) : Parcelable
}