package com.example.technicalassestment.model

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

class PopularMovies {
    data class Result(
        @field:Json(name = "results") var data: List<Data>? = arrayListOf()
    )

    @Parcelize
    data class Data(
        @field:Json(name = "id") var Id: Int? = 0,
        @field:Json(name = "title") var title: String? = "",
        @field:Json(name = "overview") var description: String? = "",
        @field:Json(name = "release_date") var releaseDate: String? = "",
        @field:Json(name = "poster_path") var posterUrl: String? = "",
        @field:Json(name = "backdrop_path") var backdropUrl: String? = "",
    ):Parcelable
}