package com.example.technicalassestment.model

import com.squareup.moshi.Json

class Qasir {
    data class Result(
        @field:Json(name = "record") var data: Data? = Data()

    )

    data class Data(
        @field:Json(name = "banner") var banner: String? = "",
        @field:Json(name = "products") var products: List<Product>? = arrayListOf(),
    )

    data class Product(
        @field:Json(name = "product_id") var productId: Int? = 0,
        @field:Json(name = "product_name") var productName: String? = "",
        @field:Json(name = "price") var price: Int? = 0,
        @field:Json(name = "stock") var stock: Int? = 0,
        @field:Json(name = "description") var description: String? = "",
        @field:Json(name = "images") var imageData: Image? = Image(),
    )

    data class Image(
        @field:Json(name = "thumbnail") var thumbnail: String? = "",
        @field:Json(name = "large") var large: String? = "",
    )
}