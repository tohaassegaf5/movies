package com.example.technicalassestment.widget

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

fun View.setViewVisibility(show: Boolean) {
    visibility = if (show) View.VISIBLE else View.GONE
}

fun View.showFadeIn() {
    if (!isVisible) {
        // Prepare the View for the animation
        isVisible = true
        alpha = 0.0f

        // Start the animation
        animate()
            .alpha(1.0f)
            .setListener(null)
    }
}

fun View.goneFadeOut() {
    if (isVisible) {
        animate()
            .alpha(0.0f)
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    super.onAnimationEnd(animation)
                    isVisible = false
                }
            })
    }
}

fun AppCompatActivity.replaceFragment(frameId: Int, fragment: Fragment, tag: String) {
    supportFragmentManager.inTransaction {
        replace(
            frameId,
            fragment,
            tag
        ).disallowAddToBackStack()
    }
}
fun Fragment.addChildFragment(frameId: Int, fragment: Fragment) {
    childFragmentManager.inTransaction { add(frameId, fragment) }
}

inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction().func().commit()
}
