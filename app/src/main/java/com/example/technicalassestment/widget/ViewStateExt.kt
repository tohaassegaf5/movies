package com.example.technicalassestment.widget

import android.view.View
import com.example.technicalassestment.R
import com.example.technicalassestment.databinding.ViewStateBinding

//var shimmerView:Shimmer


fun ViewStateBinding.setErrorLayoutEnable(
    mainContainer: View?,
    isErrorBlocking: Boolean,
    isErrorUnknown: Boolean,
) {
    this.layoutLoading.goneFadeOut()
    this.containerLoading.showFadeIn()

    if (isErrorBlocking) {
        mainContainer?.goneFadeOut()
        this.layoutError.showFadeIn()
        if (isErrorUnknown) {
            this.imgErrorResponse.setImageResource(R.drawable.ic_unknown_error)
        }
    } else {
        mainContainer?.showFadeIn()
        this.layoutError.goneFadeOut()
    }
}

fun ViewStateBinding.setLoadingEnable(
    mainContainer: View?
) {
    mainContainer?.setViewVisibility(false)
    containerLoading.showFadeIn()
}

fun ViewStateBinding.setMainContainerEnable(mainContainer: View?) {
    mainContainer?.showFadeIn()
    this.containerLoading.goneFadeOut()
}
//
//fun ViewStateBinding.showHideShimmer(id: Int) {
//    getShimmers().forEach { shimmer ->
//        if (shimmer.id == id) {
//            shimmer.setViewVisibility(true)
//            shimmerView = shimmer as ShimmerFrameLayout
//            shimmerView!!.startShimmer()
//        } else {
//            shimmer.setViewVisibility(false)
//        }
//    }
//}
