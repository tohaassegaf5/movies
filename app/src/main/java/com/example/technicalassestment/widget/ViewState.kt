package com.example.technicalassestment.widget

data class ViewState(
    var stateStatus: Status = Status.GONE,
    var isUnknownError: Boolean = false,
    var response: Response = Response.DEFAULT
) {
    enum class Status {
        PROGRESSING,
        UNKNOWN,
        GONE
    }

    enum class Response {
        DEFAULT, GET_LIST, GET_POPULAR, GET_TOP_RATED, GET_NOW_PLAYING, GET_DETAIL
    }

    enum class ViewStateType {
        DEFAULT
    }

    interface RetryRequest {
        fun retry(response: Response)
    }
}

