package com.example.technicalassestment

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatDelegate
import androidx.multidex.MultiDexApplication
import coil.Coil
import coil.imageLoader
import com.example.technicalassestment.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

open class ProjectApplication : MultiDexApplication(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)

       DaggerAppComponent.builder()
           .application(this)
           .baseUrl("https://api.themoviedb.org/3/")
           .build()
           .inject(this)
        Coil.setImageLoader(applicationContext.imageLoader)
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return androidInjector
    }

    /**
     * Handle RxJava Fatal Exception: io.reactivex.exceptions.OnErrorNotImplementedException
     *https://proandroiddev.com/default-error-handling-with-rxjava-89b79de3f125
     */
}