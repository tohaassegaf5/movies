package com.example.technicalassestment.di.module

import android.app.Application
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import coil.ImageLoader
import coil.disk.DiskCache
import com.example.technicalassestment.R
import com.example.technicalassestment.util.Constant
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {
    @Singleton
    @Provides
    fun providePreferences(application: Application): SharedPreferences =
        application.getSharedPreferences(Constant.Config.APP_PREFERENCE, MODE_PRIVATE)

    @Provides
    @Singleton
    fun provideContext(application: Application): Context = application.applicationContext

    @Provides
    @Singleton
    fun provideCoilImageLoader(application: Application): ImageLoader {
        return ImageLoader.Builder(application.applicationContext)
            .crossfade(true)
            .placeholder(R.color.grey1)
            .diskCache {
                DiskCache.Builder()
                    .directory(application.applicationContext.cacheDir.resolve("image_cache"))
                    .maxSizePercent(0.05)
                    .build()
            }
            .build()
    }
}