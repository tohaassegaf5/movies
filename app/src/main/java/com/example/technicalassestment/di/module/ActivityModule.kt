package com.example.technicalassestment.di.module

import com.example.technicalassestment.ui.cart.CartActivity
import com.example.technicalassestment.ui.checkout.CheckoutActivity
import com.example.technicalassestment.ui.home.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {
    @ContributesAndroidInjector
    internal abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun contributeCartActivity(): CartActivity

    @ContributesAndroidInjector
    internal abstract fun contributeCheckoutActivity(): CheckoutActivity

}