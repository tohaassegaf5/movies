package com.example.technicalassestment.di

import android.app.Application
import com.example.technicalassestment.ProjectApplication
import com.example.technicalassestment.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

/**
 * https://www.raywenderlich.com/262-dependency-injection-in-android-with-dagger-2-and-kotlin
 */

@Singleton
@Component(
    modules = [
        (AndroidSupportInjectionModule::class),
        (NetworkModule::class),
        (ViewModelModule::class),
        (FragmentModule::class),
        (ActivityModule::class),
        (AppModule::class)
    ]
)


interface AppComponent {
    //Create instance of this component
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        @BindsInstance
        fun baseUrl(url:String):Builder

        fun build(): AppComponent
    }

    fun inject(instance:ProjectApplication)
}