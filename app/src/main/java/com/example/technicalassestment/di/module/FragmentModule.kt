package com.example.technicalassestment.di.module

import com.example.technicalassestment.ui.cart.CartFragment
import com.example.technicalassestment.ui.checkout.CheckoutFragment
import com.example.technicalassestment.ui.home.HomeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    internal abstract fun contributeHomeFragment(): HomeFragment

    @ContributesAndroidInjector
    internal abstract fun contributeCartFragment(): CartFragment

    @ContributesAndroidInjector
    internal abstract fun contributeCheckoutFragment(): CheckoutFragment
}