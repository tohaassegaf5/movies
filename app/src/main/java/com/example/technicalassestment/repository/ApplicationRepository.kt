package com.example.technicalassestment.repository

import android.content.SharedPreferences
import com.example.technicalassestment.model.PopularMovies
import com.example.technicalassestment.model.Reviews
import com.example.technicalassestment.network.ApiService
import com.example.technicalassestment.util.Constant
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ApplicationRepository @Inject constructor(
    private val api: ApiService,
    val sharedPreferences: SharedPreferences
) {
    fun getKey(): String = Constant.Config.API_KEY
}