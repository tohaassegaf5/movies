package com.example.technicalassestment.base

import android.view.View
import androidx.databinding.ViewDataBinding
import com.example.technicalassestment.R
import com.example.technicalassestment.databinding.ViewStateBinding
import com.example.technicalassestment.util.Constant
import com.example.technicalassestment.widget.ViewState


abstract class BaseFullStateActivity<T : ViewModel, B : ViewDataBinding> : BaseActivity<T, B>() {

    override fun getToolbarColor(): Int = R.color.primary_color
    override fun getViewStateBinding(): ViewStateBinding? = null
    override fun getMainContainer(): View? = null
    override fun getRetryRequest(): ViewState.RetryRequest? = null

    override fun getToolbarTitle(): String {
        return if (intent?.extras != null && intent.extras?.containsKey(Constant.EXTRAS.EXTRAS_TOOLBAR_TITLE) == true) {
            intent.extras?.getString(Constant.EXTRAS.EXTRAS_TOOLBAR_TITLE) ?: ""
        } else {
            ""
        }
    }
}