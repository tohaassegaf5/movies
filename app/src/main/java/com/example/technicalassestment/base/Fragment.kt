package com.example.technicalassestment.base

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.example.technicalassestment.R
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.technicalassestment.databinding.ViewStateBinding
import com.example.technicalassestment.repository.ApplicationRepository
import com.example.technicalassestment.widget.ViewState
import com.example.technicalassestment.widget.setErrorLayoutEnable
import com.example.technicalassestment.widget.setLoadingEnable
import com.example.technicalassestment.widget.setMainContainerEnable
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

abstract class Fragment<T : ViewModel, VB : ViewDataBinding> : Fragment() {
    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var sharedPreferences: SharedPreferences

    @Inject
    lateinit var repository: ApplicationRepository

    lateinit var mViewModel: T
    lateinit var mViewDataBinding: VB
    var mViewStateType = ViewState.ViewStateType.DEFAULT

    private var isGotoSignin = false

    abstract fun setViewModel()
    abstract fun getContentView(): Int
    abstract fun getMainContainer(): View?
    abstract fun getViewStateBinding(): ViewStateBinding?
    abstract fun getRetryRequest(): ViewState.RetryRequest?

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidSupportInjection.inject(this)
        setViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mViewDataBinding = DataBindingUtil.inflate(inflater, getContentView(), container, false)
        mViewDataBinding.lifecycleOwner = this
        return mViewDataBinding.root
    }

    override fun onDestroyView() {
        mViewModel.mutableViewState.removeObservers(viewLifecycleOwner)
        mViewDataBinding.invalidateAll()
        super.onDestroyView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handlingViewState(getMainContainer(), getViewStateBinding(), getRetryRequest())
    }

    private fun handlingViewState(
        mainContainer: View?, viewStateBinding: ViewStateBinding?,
        retryRequest: ViewState.RetryRequest?
    ) {
        mViewModel.mutableViewState.observe(viewLifecycleOwner) { viewState ->
            when (viewState.stateStatus) {
                ViewState.Status.PROGRESSING -> {
                    viewStateBinding?.setLoadingEnable(mainContainer)
                }
                ViewState.Status.UNKNOWN -> {
                    viewStateBinding?.setErrorLayoutEnable(
                        mainContainer,
                        isErrorBlocking = true,
                        viewState.isUnknownError
                    )
                    if (viewState.isUnknownError) {
                        viewStateBinding?.btnRetry?.setOnClickListener {
                            retryRequest?.retry(viewState.response)
                        }
                    }
                }
                else -> {
                    viewStateBinding?.setMainContainerEnable(mainContainer)
                }
            }
        }
    }

    fun <T : Fragment> getFragment(fragmentId: Int): T? {
        return childFragmentManager.findFragmentById(fragmentId) as? T
    }

    fun newInstanceFragment(bundle: Bundle, fragment: Fragment): Fragment {
        fragment.arguments = bundle
        return fragment
    }

    fun startNavigationTo(activityTarget: Class<*>, bundle: Bundle) {
        val intent = Intent(activity, activityTarget)
        intent.putExtras(bundle)
        startActivity(intent)
        animationOpenActivity()
    }

    fun animationOpenActivity() {
        activity?.overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }

    fun checkRequireArguments(
        key: String,
        doOnNext: (String) -> Unit,
        shouldRemoveKey: Boolean = false
    ) {
        if (arguments?.containsKey(key) == true) {
            doOnNext(key)
            if (shouldRemoveKey) arguments?.remove(key)

        }
    }

}