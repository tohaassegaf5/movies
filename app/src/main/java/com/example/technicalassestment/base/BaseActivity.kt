package com.example.technicalassestment.base

import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.ColorRes
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.technicalassestment.R
import com.example.technicalassestment.databinding.ViewStateBinding
import com.example.technicalassestment.listener.ConnectionChangeListener
import com.example.technicalassestment.service.ConnectionService
import com.example.technicalassestment.widget.*
import dagger.android.AndroidInjection
import javax.inject.Inject

abstract class BaseActivity<T : ViewModel, VB : ViewDataBinding> : AppCompatActivity(),
    ConnectionChangeListener {

    lateinit var mConnectionService: ConnectionService
    lateinit var mViewModel: T
    lateinit var mViewDataBinding: VB

    var intentFilter = IntentFilter()
    var isConnected = true
    var viewNoConnection: View? = null


    @Inject
    lateinit var sharedPreferences: SharedPreferences

    @Inject
    lateinit var mViewModelFactory: ViewModelProvider.Factory

    /**
     * Handling initialize for ViewModel and add into mViewModel
     */
    abstract fun setViewModel()

    /**
     * Handle main view in activity / fragment,
     * set into function setContentView(here)
     */
    abstract fun getContentView(): Int

    /**
     * Initialize ViewState(loading page) to handle in each view
     */
    abstract fun getViewStateBinding(): ViewStateBinding?

    /**
     * Initialize main container and call in front activity
     */
    abstract fun getFragmentContainer(): Fragment?

    /**
     * Initialize view state container for handling VISIBLE || GONE
     */
    abstract fun getMainContainer(): View?

    /**
     * Listener for handle button retry in ViewState
     */
    abstract fun getRetryRequest(): ViewState.RetryRequest?

    /**
     * Set toolbar title
     */
    abstract fun getToolbarTitle(): String

    /**
     * Custom toolbar background color if need
     */
    @ColorRes
    abstract fun getToolbarColor(): Int

    companion object {
        fun newInstanceFragment(bundle: Bundle, fragment: Fragment): Fragment {
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
        mConnectionService = ConnectionService(this)
        mConnectionService.connectionListener = this
        mViewDataBinding = DataBindingUtil.setContentView(this, getContentView())
        mViewDataBinding.lifecycleOwner = this
        setViewModel()
        handlingViewState(getMainContainer(), getViewStateBinding(), getRetryRequest())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            disableAutoFillforO()
        }
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(mConnectionService, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(mConnectionService)
    }

    override fun onDestroy() {
        mViewModel.mutableViewState.removeObservers(this)
        mViewDataBinding.invalidateAll()
        super.onDestroy()
    }

    override fun onConnectionChange(isConnected: Boolean) {
        handleConnectionChange(isConnected)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun disableAutoFillforO() {
        window.decorView.importantForAutofill = View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS
    }


    fun handleConnectionChange(value: Boolean) {
        this.isConnected = value
        if (viewNoConnection != null) {
            viewNoConnection?.setViewVisibility(!isConnected)
        }
    }

    fun animationOpenActivity() {
        overridePendingTransition(R.anim.anim_slide_in_left, R.anim.anim_slide_out_left)
    }

    fun animationCloseActivity() {
        overridePendingTransition(R.anim.anim_slide_out_right, R.anim.anim_slide_in_right)
    }


    private fun handlingViewState(
        mainContainer: View?,
        viewStateBinding: ViewStateBinding?,
        retryRequest: ViewState.RetryRequest?
    ) {
        mViewModel.mutableViewState.observe(this) { viewState ->
            when (viewState.stateStatus) {
                ViewState.Status.PROGRESSING -> {
                    viewStateBinding?.setLoadingEnable(mainContainer)
                }
//                ViewState.Status.ERROR -> {
////                    viewStateBinding?.setErrorLayoutEnable(
////                        mainContainer,
////                        viewState.isErrorBlocking,
////                        !viewState.isErrorUnknown,
////                        if (!viewState.stringMessage.isNullOrEmpty()) viewState.stringMessage else "Sepertinya terjadi kesalahan, silakan coba lagi"
////                    )
////                    if (viewState.isErrorBlocking) {
////                        viewStateBinding?.btnRetry?.setOnClickListener {
////                            retryRequest?.retry(viewState.response)
////                        }
////                    } else {
////                        //TODO handle non blocking
////                    }
//                }
//                ViewState.Status.UNAUTHORIZED -> {
//                    viewStateBinding?.setMainContainerEnable(mainContainer)
//                    retryRequest?.handleUnauthorized()
//                }
//                ViewState.Status.OBSOLETED -> {
//                    viewStateBinding?.setMainContainerEnable(mainContainer)
//                    obsoleteVersionApps()
//                }
                ViewState.Status.UNKNOWN -> {
                    viewStateBinding?.setErrorLayoutEnable(
                        mainContainer,
                        isErrorBlocking = false,
                        viewState.isUnknownError,

                        )
                    if (viewState.isUnknownError) {
                        viewStateBinding?.btnRetry?.setOnClickListener {
                            retryRequest?.retry(viewState.response)
                        }
                    } else {
                        //TODO handle non blocking
                    }
                }
                else -> {
                    viewStateBinding?.setMainContainerEnable(mainContainer)
                }
            }
        }
    }

    fun startNavigationTo(activityTarget: Class<*>, bundle: Bundle) {
        val intent = Intent(this, activityTarget)
        intent.putExtras(bundle)
        startActivity(intent)
        animationOpenActivity()
    }
}