package com.example.technicalassestment.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.technicalassestment.widget.ViewState
import io.reactivex.disposables.CompositeDisposable

abstract class ViewModel : ViewModel() {
    val apiResponse: MutableLiveData<ViewState.Response> = MutableLiveData()
    val mutableViewState: MutableLiveData<ViewState> = MutableLiveData()
    var compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        compositeDisposable.clear()
        compositeDisposable.dispose()
        super.onCleared()
    }
}