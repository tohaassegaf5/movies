package com.example.technicalassestment.network

import androidx.lifecycle.MutableLiveData
import com.example.technicalassestment.util.Constant
import com.example.technicalassestment.widget.ViewState
import com.google.gson.Gson
import com.google.gson.JsonObject
import io.reactivex.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import retrofit2.HttpException

abstract class ApiResponse<T>(
    compositeDisposable: CompositeDisposable,
    mutableViewState: MutableLiveData<ViewState>,
    private val response: ViewState.Response
) : Observer<T> {

    private val viewState = ViewState()
    private var mldViewState: MutableLiveData<ViewState> = MutableLiveData()
    private var cd = CompositeDisposable()

    private val unknownErrorMessage =
        "Halaman yang kamu tuju tidak dapat ditampilkan, silahkan ulangi beberapa saat lagi."
    private val connectionErrorMessage = "Periksa koneksi internet kamu dan silahkan ulangi lagi."

    init {
        mldViewState = mutableViewState
        cd = compositeDisposable
    }

    private fun getSubscribe(): CompositeDisposable {
        if (cd.isDisposed) {
            cd = CompositeDisposable()
        }
        return cd
    }

    fun onStateLoading() {
        viewState.stateStatus = ViewState.Status.PROGRESSING
        mldViewState.value = viewState
    }

    fun onStateGone() {
        viewState.stateStatus = ViewState.Status.GONE
        mldViewState.value = viewState
    }


    fun onSetBlacklistRequest(t: T) {
        //TODO will override
    }

    fun onStateMaintenance(message: String?) {
        //TODO using event bus
    }

    override fun onSubscribe(d: Disposable) {
        onStateLoading()
        getSubscribe().add(d)
    }

    override fun onError(e: Throwable) {
        if (e is HttpException) {
            handleErrorCode(e.code(), unknownErrorMessage)
        }
        if (e is ApiException) {
            if (e.type == ApiException.Type.NETWORK) {
                // handleErrorCode(null, "Error koneksi")
                handleErrorCode(null, unknownErrorMessage)
            }
            if (e.type == ApiException.Type.HTTP) {
                handleErrorCode(e.response?.code(), unknownErrorMessage)
            }
            if (e.type == ApiException.Type.UNEXPECTED) {
                // handleErrorCode(null, "Unexpected Error")
                handleErrorCode(null, unknownErrorMessage)
            }
        } else {
            onErrorRequest(unknownErrorMessage)
        }
        viewState.stateStatus = ViewState.Status.UNKNOWN
        viewState.isUnknownError = true
        mldViewState.value = viewState
    }

    override fun onNext(it: T) {
        onSucceedRequest(it, response)
        onStateGone()
    }

    override fun onComplete() {}

    private fun handleErrorCode(e: Int?, s: String?) {
        if (e != null) {
            when (e) {
                in 300..399 -> {
                    //onErrorRequest("Oops.. request telah dialihkan")
                    onErrorRequest(s)
                }
                in 400..499 -> {
                    //onErrorRequest("Oops.. terjadi kesalahan pada permintaan anda")
                    onErrorRequest(s)
                }
                in 500..599 -> {
                    //onErrorRequest("Oops.. terjadi kesalahan pada server, silakan coba lagi")
                    onErrorRequest(s)
                }
                else -> {
                    // onErrorRequest("Oops, sepertinya terjadi kesalahan\nsaat menghubungkan. Silahkan coba lagi…")
                    onErrorRequest(s)
                }

            }
        }
        if (s != null) {
            onErrorRequest(s)
        }
    }

    abstract fun onSucceedRequest(it: T, response: ViewState.Response)
    abstract fun onErrorRequest(message: String?)
}