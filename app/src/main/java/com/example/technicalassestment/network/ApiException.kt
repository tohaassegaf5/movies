package com.example.technicalassestment.network

import io.reactivex.annotations.NonNull
import io.reactivex.annotations.Nullable
import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException

class ApiException constructor(
    @NonNull psMessage: String?, @param:Nullable
    /**
     * The request URL which produced the error.
     */
    val url: String?,
    @param:Nullable
    /**
     * Response object containing status code, headers, body, etc.
     */
    val response: Response<*>?, @param:NonNull
    /**
     * The event kind which triggered this error.
     */
    val type: Type?,
    @Nullable poException: Throwable?, @param:Nullable
    /**
     * The Retrofit this request was executed on
     */
    val retrofit: Retrofit?
) : RuntimeException(psMessage, poException) {

    /**
     * Identifies the event kind which triggered a [ApiException].
     */
    enum class Type {
        /**
         * An [IOException] occurred while communicating to the server.
         */
        NETWORK,

        /**
         * A non-200 HTTP status code was received from the server.
         */
        HTTP,

        /**
         * An internal error occurred while attempting to execute a request. It is best practice to
         * re-throw this exception so your application crashes.
         */
        UNEXPECTED
    }

    /**
     * HTTP response body converted to specified `poType`. `null` if there is no
     * response.
     *
     * @throws IOException if unable to convert the body to the specified `poType`.
     */
    @Throws(IOException::class)
    fun <T> getErrorBodyAs(@NonNull poType: Class<T>): T? {
        if (response?.errorBody() == null || retrofit == null) {
            return null
        }
        val loConverter = retrofit.responseBodyConverter<T>(poType, arrayOfNulls(0))
        return loConverter.convert(response.errorBody()!!)
    }

    companion object {

        fun httpError(
            @NonNull psUrl: String,
            @NonNull poResponse: Response<*>,
            @NonNull poRetrofit: Retrofit
        ): ApiException {
            val lsMessage = poResponse.code().toString() + " " + poResponse.message()
            return ApiException(
                lsMessage,
                psUrl,
                poResponse,
                Type.HTTP,
                null,
                poRetrofit
            )
        }

        fun networkError(@NonNull poException: IOException): ApiException {
            return ApiException(
                poException.message,
                null,
                null,
                Type.NETWORK,
                poException,
                null
            )
        }

        fun unexpectedError(@NonNull poException: Throwable): ApiException {
            return ApiException(
                poException.message,
                null,
                null,
                Type.UNEXPECTED,
                poException,
                null
            )
        }
    }
}