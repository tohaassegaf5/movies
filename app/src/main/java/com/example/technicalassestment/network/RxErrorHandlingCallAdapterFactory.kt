package com.example.technicalassestment.network

import io.reactivex.Observable
import io.reactivex.ObservableSource
import io.reactivex.functions.Function
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.HttpException
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.io.IOException
import java.lang.reflect.Type

class RxErrorHandlingCallAdapterFactory : CallAdapter.Factory() {
    private val original: RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create()

    override fun get(returnType: Type, annotations: Array<Annotation>, retrofit: Retrofit): CallAdapter<*, *> {
        val wrapped = original.get(returnType, annotations, retrofit) as CallAdapter<out Any, *>
        return RxCallAdapterWrapper(
            retrofit,
            wrapped
        )
    }

    private class RxCallAdapterWrapper<R>(
        val retrofit: Retrofit,
        val wrapped: CallAdapter<R, *>
    ) : CallAdapter<R, Observable<R>> {

        override fun responseType(): Type {
            return wrapped.responseType()
        }

        override fun adapt(call: Call<R>): Observable<R> {
            return (wrapped.adapt(call) as Observable<R>)
                .onErrorResumeNext(Function<Throwable, ObservableSource<R>> { throwable ->
                    Observable.error(
                        asRetrofitException(throwable)
                    )
                })
        }

        private fun asRetrofitException(throwable: Throwable): ApiException {
            // We had non-200 http error
            if (throwable is HttpException) {
                val response = throwable.response()
                return ApiException.httpError(
                    response!!.raw().request.url.toString(),
                    response,
                    retrofit
                )
            }
            // A network error happened
            return if (throwable is IOException) {
                ApiException.networkError(throwable)
            } else ApiException.unexpectedError(throwable)

            // We don't know what happened. We need to simply convert to an unknown error

        }
    }

    companion object {
        fun create(): CallAdapter.Factory {
            return RxErrorHandlingCallAdapterFactory()
        }
    }
}