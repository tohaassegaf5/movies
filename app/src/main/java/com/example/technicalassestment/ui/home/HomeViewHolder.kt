package com.example.technicalassestment.ui.home

import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.technicalassestment.databinding.ItemHomeBinding
import com.example.technicalassestment.extension.convertToCurrencyFormat
import com.example.technicalassestment.model.Product
import com.example.technicalassestment.model.Qasir

class HomeViewHolder constructor(val binding: ItemHomeBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(item: Product.Data) {
        binding.apply {
//            ivItem.load(item.imageData?.thumbnail.orEmpty())
            tvTitle.text = item.name
            tvPrice.text = item.price.toString().convertToCurrencyFormat(true)
            tvStockCount.text = item.stock.toString()
        }

    }
}