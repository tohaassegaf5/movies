package com.example.technicalassestment.ui.cart

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import com.example.technicalassestment.R
import com.example.technicalassestment.base.Fragment
import com.example.technicalassestment.databinding.FragmentCartBinding
import com.example.technicalassestment.databinding.ViewStateBinding
import com.example.technicalassestment.extension.convertToCurrencyFormat
import com.example.technicalassestment.model.Product
import com.example.technicalassestment.ui.checkout.CheckoutActivity
import com.example.technicalassestment.ui.checkout.CheckoutFragment
import com.example.technicalassestment.util.Constant
import com.example.technicalassestment.widget.ViewState

class CartFragment : Fragment<CartViewModel, FragmentCartBinding>() {
    var data: Product.Data? = null
    var totalProduct = 0

    companion object {
        const val EXTRAS_DATA_PRODUCT = "extras_product_data"
    }

    override fun setViewModel() {
        mViewModel = ViewModelProvider(this, mViewModelFactory).get(CartViewModel::class.java)
        checkRequireArguments(EXTRAS_DATA_PRODUCT, {
            data = requireArguments().getParcelable(it) ?: Product.Data()
        })
    }

    override fun getContentView(): Int = R.layout.fragment_cart

    override fun getMainContainer(): View? = mViewDataBinding.containerMain

    override fun getViewStateBinding(): ViewStateBinding? = null

    override fun getRetryRequest(): ViewState.RetryRequest? = null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeData()
        mViewDataBinding.apply {
            tvProductTotal.text = totalProduct.toString()
            tvTitle.text = data?.name
            tvTotalPrice.text =
                (totalProduct * data?.price!!).toString().convertToCurrencyFormat(true)

            tvPrice.text = data?.price.toString().convertToCurrencyFormat(true)
            btnMin.setOnClickListener {
                totalProduct--
                mViewModel.calculate(totalProduct)
            }
            btnPlus.setOnClickListener {
                totalProduct++
                mViewModel.calculate(totalProduct)
            }
            btnNextPayment.setOnClickListener {
//                val item = mAdapter?.getItemPosition(position)
                val result=Product.Data(name = data?.name?:"",price = data?.price?:0,quantity = totalProduct,totalPrice = (totalProduct * data?.price!!))
                val bundle = bundleOf(CheckoutFragment.EXTRAS_DATA_PRODUCT to result, Constant.EXTRAS.EXTRAS_TOOLBAR_TITLE to "Cart")
                startNavigationTo(CheckoutActivity::class.java, bundle)

            }
            toolbar.setNavigationOnClickListener { requireActivity().finish() }
        }
    }

    private fun observeData() {
        mViewModel.mldTotalProduct.observe(viewLifecycleOwner) {
            mViewDataBinding.tvProductTotal.text = it.toString()
            mViewDataBinding.btnMin.isEnabled = it.toInt() > 0
            mViewDataBinding.btnPlus.isEnabled = it.toInt() < data?.stock ?: 0
            mViewDataBinding.tvTotalPrice.text =
                (it * data?.price!!).toString().convertToCurrencyFormat(true)
        }
    }
}