package com.example.technicalassestment.ui.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.technicalassestment.R
import com.example.technicalassestment.databinding.ItemHomeBinding
import com.example.technicalassestment.model.Product
import com.example.technicalassestment.model.Qasir

interface OnItemClickListener {
    fun onItemClicked(view: View, position: Int)
}

class HomeAdapter constructor(val items: List<Product.Data>) :
    RecyclerView.Adapter<HomeViewHolder>() {

    lateinit var listener: OnItemClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        val binding: ItemHomeBinding =
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.item_home,
                parent,
                false
            )
        return HomeViewHolder(binding)
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        holder.bind(items[position])
        holder.itemView.setOnClickListener { listener.onItemClicked(it, position) }
    }

    override fun getItemCount(): Int = items.size

    fun setOnItemClickListener(listener: OnItemClickListener) {
        this.listener = listener
    }

    fun getItemPosition(position: Int): Product.Data = items[position]
}
