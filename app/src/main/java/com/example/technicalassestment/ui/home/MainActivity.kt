package com.example.technicalassestment.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.technicalassestment.R
import com.example.technicalassestment.base.BaseFullStateActivity
import com.example.technicalassestment.databinding.ActivityMainBinding
import com.example.technicalassestment.widget.replaceFragment

class MainActivity : BaseFullStateActivity<EmptyViewModel, ActivityMainBinding>() {
    override fun setViewModel() {
        mViewModel =
            ViewModelProvider(this, mViewModelFactory).get(EmptyViewModel::class.java)
    }

    override fun getContentView(): Int = R.layout.activity_main

    override fun getFragmentContainer(): Fragment? {
        return newInstanceFragment(intent.extras ?: Bundle(), HomeFragment())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewNoConnection = mViewDataBinding.layoutNoConnection.layoutError
        replaceFragment(
            R.id.container_fragment,
            getFragmentContainer()!!,
            getFragmentContainer()!!::class.java.simpleName
        )
    }
}