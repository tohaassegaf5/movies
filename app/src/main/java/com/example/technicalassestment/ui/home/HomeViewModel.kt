package com.example.technicalassestment.ui.home

import com.example.technicalassestment.base.ViewModel
import com.example.technicalassestment.model.Product
import com.example.technicalassestment.repository.ApplicationRepository
import javax.inject.Inject

class HomeViewModel @Inject constructor(val repository: ApplicationRepository) : ViewModel(){

    fun getProducts(): List<Product.Data> {
        return listOf(
            Product.Data("Ombak Water Purrifier", 5, 20000000),
            Product.Data("Villaem Water Purrifier", 10, 18000000),
            Product.Data("Core Water Purrifier", 12, 17000000),
            Product.Data("Triple Power Air Purrifier", 21, 10000000),
            Product.Data("Storm Air Purrifier", 11, 9000000),
            Product.Data("Breeze", 4, 8000000),
        )
    }
}