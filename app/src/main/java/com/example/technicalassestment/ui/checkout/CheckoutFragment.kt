package com.example.technicalassestment.ui.checkout

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.example.technicalassestment.R
import com.example.technicalassestment.base.Fragment
import com.example.technicalassestment.databinding.FragmentCheckoutBinding
import com.example.technicalassestment.databinding.ViewStateBinding
import com.example.technicalassestment.extension.convertToCurrencyFormat
import com.example.technicalassestment.model.Product
import com.example.technicalassestment.ui.home.MainActivity
import com.example.technicalassestment.widget.ViewState

class CheckoutFragment : Fragment<CheckoutViewModel, FragmentCheckoutBinding>() {

    var data: Product.Data? = null

    companion object {
        const val EXTRAS_DATA_PRODUCT = "extras_product_data_checkout"
    }

    override fun setViewModel() {
        mViewModel = ViewModelProvider(this, mViewModelFactory).get(CheckoutViewModel::class.java)
        checkRequireArguments(EXTRAS_DATA_PRODUCT, {
            data = requireArguments().getParcelable(it) ?: Product.Data()
        })
    }

    override fun getContentView(): Int = R.layout.fragment_checkout

    override fun getMainContainer(): View? = mViewDataBinding.containerMain

    override fun getViewStateBinding(): ViewStateBinding? = null

    override fun getRetryRequest(): ViewState.RetryRequest? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewDataBinding.apply {
            tvTitle.text = data?.name ?: ""
            tvTotal.text = "qty : ${(data?.quantity ?: 0)}"
            tvPrice.text = (data?.price ?: 0).toString().convertToCurrencyFormat(true)
            tvTotalPrice.text = (data?.totalPrice ?: 0).toString().convertToCurrencyFormat(true)
            btnNextPayment.setOnClickListener {
                Toast.makeText(requireContext(), "Pembelian Berhasil", Toast.LENGTH_SHORT).show()
                val intent = Intent(requireContext(), MainActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                startActivity(intent)
            }
            tvTotalPriceDetail.text =
                (data?.totalPrice ?: 0).toString().convertToCurrencyFormat(true)
            tvPriceDetail.text = (data?.price ?: 0).toString().convertToCurrencyFormat(true)
            tvQty.text = data?.quantity.toString()
            toolbar.setNavigationOnClickListener { requireActivity().finish() }
        }
    }
}