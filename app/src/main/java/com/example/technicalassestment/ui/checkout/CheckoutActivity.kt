package com.example.technicalassestment.ui.checkout

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.technicalassestment.R
import com.example.technicalassestment.base.BaseFullStateActivity
import com.example.technicalassestment.databinding.ActivityDetailBinding
import com.example.technicalassestment.ui.cart.CartFragment
import com.example.technicalassestment.ui.home.EmptyViewModel
import com.example.technicalassestment.widget.replaceFragment

class CheckoutActivity : BaseFullStateActivity<EmptyViewModel, ActivityDetailBinding>() {
    override fun setViewModel() {
        mViewModel =
            ViewModelProvider(this, mViewModelFactory).get(EmptyViewModel::class.java)
    }

    override fun getContentView(): Int = R.layout.activity_detail

    override fun getFragmentContainer(): Fragment? {
        val fragment = CheckoutFragment()
        val bundle = intent.extras ?: Bundle()
        fragment.arguments = bundle
        return newInstanceFragment(bundle, fragment)
    }

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewNoConnection = mViewDataBinding.layoutNoConnection.layoutError
        replaceFragment(
            R.id.container_fragment,
            getFragmentContainer()!!,
            getFragmentContainer()!!::class.java.simpleName
        )
    }

}