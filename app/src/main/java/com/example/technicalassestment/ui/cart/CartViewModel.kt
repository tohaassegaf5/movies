package com.example.technicalassestment.ui.cart

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.technicalassestment.base.ViewModel
import javax.inject.Inject

class CartViewModel @Inject constructor() : ViewModel() {

    private var _mldTotalProduct: MutableLiveData<Int> = MutableLiveData()
    var mldTotalProduct: LiveData<Int> = _mldTotalProduct

    fun calculate(pcs: Int?) {
        _mldTotalProduct.value = pcs
    }

}