package com.example.technicalassestment.ui.home

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import com.example.technicalassestment.R
import com.example.technicalassestment.base.Fragment
import com.example.technicalassestment.databinding.FragmentHomeBinding
import com.example.technicalassestment.databinding.ViewStateBinding
import com.example.technicalassestment.ui.cart.CartActivity
import com.example.technicalassestment.ui.cart.CartFragment
import com.example.technicalassestment.util.Constant
import com.example.technicalassestment.widget.ViewState

class HomeFragment : Fragment<HomeViewModel, FragmentHomeBinding>(), OnItemClickListener {

    private var mAdapter: HomeAdapter? = null
    override fun setViewModel() {
        mViewModel = ViewModelProvider(this, mViewModelFactory).get(HomeViewModel::class.java)
    }

    override fun getContentView(): Int = R.layout.fragment_home

    override fun getMainContainer(): View? = mViewDataBinding.containerMain

    override fun getViewStateBinding(): ViewStateBinding? = mViewDataBinding.viewState

    override fun getRetryRequest(): ViewState.RetryRequest? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val items = mViewModel.getProducts()
        mAdapter = HomeAdapter(items)
        mAdapter?.setOnItemClickListener(this)
        mViewDataBinding.ivBanner.load("https://www.1stmegasaver.com.ph/wp-content/uploads/2022/01/Archive-Banner-Air-purifier-min.jpg")
        mViewDataBinding.recyclerview.apply {
            setHasFixedSize(true)
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = mAdapter
        }

    }

    override fun onItemClicked(view: View, position: Int) {
        val item = mAdapter?.getItemPosition(position)
        val bundle = bundleOf(
            CartFragment.EXTRAS_DATA_PRODUCT to item,
            Constant.EXTRAS.EXTRAS_TOOLBAR_TITLE to "Cart"
        )
        startNavigationTo(CartActivity::class.java, bundle)
    }
}