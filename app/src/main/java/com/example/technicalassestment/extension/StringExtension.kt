package com.example.technicalassestment.extension

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

fun String.convertToCurrencyFormat(shouldAddCurrency: Boolean): String {
    val locale = Locale("in", "id")
    val symbols = DecimalFormatSymbols.getInstance(locale)
    symbols.groupingSeparator = '.'
    symbols.monetaryDecimalSeparator = ','

    if (shouldAddCurrency) {
        symbols.currencySymbol = symbols.currencySymbol
    } else {
        symbols.currencySymbol = ""
    }

    val df = DecimalFormat.getCurrencyInstance(locale) as DecimalFormat
    val kursIndonesia = DecimalFormat(df.toPattern(), symbols)
    kursIndonesia.maximumFractionDigits = 0

    return if (this.isEmpty()) {
        kursIndonesia.format(0)
    } else {
        kursIndonesia.format(java.lang.Double.parseDouble(this))
    }
}